<?php

foreach (glob(__DIR__.'/*.php') as $helper) {
    if (basename($helper) === '_load-all.php') {
        continue;
    }
    require_once($helper);
}
