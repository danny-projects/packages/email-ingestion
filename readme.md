# Email Ingestion

Processes incoming emails and allows you to route them based on various rules.

## Setup

### Installation

Install normally via composer (`danny-cain/email-ingestion`)
Schedule the `emails:process` task to run as often as you wish to process the mail queue.

### Email Setup

Pipe received emails to the `emails:ingest` artisan command, on linux you will want to create a `.forward` file in the home directory of any user whose emails should be handled with the following content:

```
|"{path-to-artisan} emails:ingest"
```

### Routing

Create an `emails.php` file in your `app/routes` directory. 

Define routes using:
```php
app(EmailIngestion::class)->route()
    ->to('/^abuse@/')
    ->handle(function (IncomingEmail $email) {
        // todo - handle the email here
    })
    ->name('abuse mailbox');
```
You can test against `to`, `from`, `subject` and `body` and can either pass in a regexp or a plain string for direct equality matching.
The first matching route will be used, the `name` property is optional, if specified the email will be tagged with the name of the handler that processed it.


You can define a fallback handler (for any email not handled by a route) with:
```php
app(EmailIngestion::class)->fallback(function(IncomingEmail $email) {

});
```
