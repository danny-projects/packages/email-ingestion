<?php

namespace DannyCain\EmailIngestion\Controllers;

use DannyCain\EmailIngestion\Enums\Permission;
use DannyCain\EmailIngestion\Models\ImapAccount;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Crypt;
use League\OAuth2\Client\Provider\AbstractProvider;

class OauthController extends Controller
{
    use AuthorizesRequests;

    public function redirect(Request $request)
    {
        $this->authorize(Permission::AuthenticateAccount);
        $input = $request->validate([
            'provider' => ['nullable', 'string'],
        ]);
        if ($providerKey = $input['provider'] ?? '') {
            $provider = $this->providerFactory($providerKey);
            $url = $provider->getAuthorizationUrl();
            session()->put("oauth-{$provider->getState()}", ['user' => user()->id]);
            return redirect($url);
        } else {
            // todo - show provider list
        }
    }

    public function auth(Request $request, string $provider)
    {
        $this->authorize(Permission::AuthenticateAccount);
        $input = $request->validate([
            'error' => ['nullable', 'string'],
            'code' => ['required_without:error', 'string'],
            'state' => ['required_without:error', 'string'],
        ]);

        if ($error = $input['error'] ?? '') {
            throw new \RuntimeException($error);
        }

        $state = session()->pull('oauth-'.$input['state'], []);
        if (($state['user'] ?? '') !== user()->id) {
            throw new \RuntimeException("State failure");
        }

        $instance = $this->providerFactory($provider);
        $config = config("email-ingestion.oauth-providers.{$provider}");

        $token = $instance->getAccessToken('authorization_code', [
            'code' => $input['code'],
        ]);
        $owner = $instance->getResourceOwner($token);
        $email = $owner->toArray()[$config['accountKey']] ?? '';
        if (!$email) {
            throw new \RuntimeException("Unable to determine email address.");
        }

        $account = ImapAccount
            ::where('email', $email)
            ->first();
        if (!$account) {
            $account = ImapAccount::make([
                'email' => $email,
            ]);
        }

        $account->fill([
            'is_active' => true,
            'inbox_folder' => $config['inboxFolder'],
            'auth_type' => 'oauth',
            'host' => $config['host'],
            'username' => $email,
            'encrypted_access_token' => Crypt::encryptString($token->getToken()),
            'encrypted_refresh_token' => Crypt::encryptString($token->getRefreshToken()),
            'token_expires_at' => carbon($token->getExpires())->setTimezone(config('app.timezone')),
        ]);
        $account->save();

        return view('email-ingestion::account-authenticated', [
            'account' => $account,
        ]);
    }

    protected function providerFactory(string $provider): AbstractProvider
    {
        $config = config("email-ingestion.oauth-providers.{$provider}");
        if (!$config) {
            throw new \RuntimeException("Unknown provider {$provider}");
        }

        return app($config['provider'], ['options' => $config['options']]);
    }
}
