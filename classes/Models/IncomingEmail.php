<?php

namespace DannyCain\EmailIngestion\Models;

use Illuminate\Database\Eloquent\Model;

class IncomingEmail extends Model
{
    public const Valid = 'valid';
    public const Invalid = 'invalid';
    public const NotChecked = 'not-checked';
    public const Error = 'error';

    protected $guarded = [];

    protected $casts = [
        'json_to' => 'array',
        'json_attachments' => 'array',
        'json_headers' => 'array',
        'was_handled' => 'bool',
        'received_at' => 'datetime',
        'started_processing_at' => 'datetime',
        'processed_at' => 'datetime',
    ];

    public function firstHeaderValue(string $name): ?string
    {
        $header = $this->json_headers[$name] ?? [];
        return $header[0]['value'] ?? null;
    }
}
