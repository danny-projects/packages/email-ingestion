<?php

namespace DannyCain\EmailIngestion\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class ImapAccount extends Model
{
    protected $guarded = [];

    protected $casts = [
        'token_expires_at' => 'datetime',
        'json_configuration' => 'array',
        'error_count' => 'integer',
        'last_checked_at' => 'datetime',
    ];

    public function requiresRefresh(): Attribute
    {
        return Attribute::make(
            get: function(): bool
            {
                return $this->auth_type === 'oauth' && now()->lte($this->token_expires_at);
            }
        );
    }

    public function isValid(): Attribute
    {
        return Attribute::make(
            get: function(): bool
            {
                if ($this->auth_type === 'oauth') {
                    try {
                        $token = Crypt::decryptString($this->encrypted_access_token);
                        if (!$token) {
                            return false;
                        }

                        return now()->gt($this->token_expires_at);
                    } catch (\Throwable) {
                        return false;
                    }
                } else {
                    try {
                        $password = Crypt::decryptString($this->encrypted_password);
                        return (bool)$password;
                    } catch (\Throwable) {
                        return false;
                    }
                }
            }
        );
    }

    public function toConfigArray(): array
    {
        // todo - trigger oauth refresh if needed
        // todo - check we have a password
        $default = config('email-ingestion.default-account', []);
        $specific = [
            'host' => $this->host,
            'protocol' => 'imap',
            'encryption' => 'ssl',
            'validate_cert' => true,
            'username' => $this->username,
            'password' => match($this->auth_type) {
                'oauth' => Crypt::decryptString($this->encrypted_access_token),
                default => Crypt::decryptString($this->encrypted_password),
            },
            'authentication' => match($this->auth_type) {
                'oauth' => 'oauth',
                default => null,
            },
        ];

        return ($this->json_config ?? []) + $specific + $default;
    }
}
