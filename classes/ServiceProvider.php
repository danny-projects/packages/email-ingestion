<?php

namespace DannyCain\EmailIngestion;

use DannyCain\EmailIngestion\Console\Emails\Imap;
use DannyCain\EmailIngestion\Console\Emails\Ingest;
use DannyCain\EmailIngestion\Console\Emails\Process;
use DannyCain\EmailIngestion\Imap\ImapClient;
use DannyCain\EmailIngestion\Support\EmailIngestion;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    private const NAME = 'email-ingestion';

    public function register()
    {
        // Config
        $configFile = __DIR__ . '/../config/' . static::NAME . '.php';
        $this->mergeConfigFrom($configFile, static::NAME);
        $this->publishes([$configFile => config_path(static::NAME . '.php')], static::NAME . '-config');

        app()->singleton(ImapClient::class);
        app()->singleton(EmailIngestion::class);
        app()->resolving(EmailIngestion::class, fn(EmailIngestion $service) => $service
            ->loadRoutesFrom(base_path('routes/emails.php'))
        );
    }

    public function boot()
    {
        // Commands
        $this->commands([
            Ingest::class,
            Process::class,
            Imap::class,
        ]);

        // Factories
        // $this->loadFactoriesFrom(__DIR__ . '/../factories');

        // Migrations
        $this->loadMigrationsFrom(__DIR__ . '/../migrations');

        // Routes
        $this->loadRoutesFrom(__DIR__ . '/../routes.php');

        // Translations
        // $langDir = __DIR__ . '/../lang';
        // $this->loadTranslationsFrom($langDir, static::NAME);
        // $this->publishes([$langDir => resource_path('lang/vendor/' . static::NAME)], static::NAME . '-lang');

        // Views
        $viewsDir = __DIR__ . '/../views';
        $this->loadViewsFrom($viewsDir, static::NAME);
        $this->publishes([$viewsDir => resource_path('views/vendor/' . static::NAME)], static::NAME . '-views');
    }
}
