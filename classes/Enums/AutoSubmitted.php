<?php

namespace DannyCain\EmailIngestion\Enums;

class AutoSubmitted
{
    public const No = 'no';
    public const AutoGenerated = 'auto-generated';
    public const AutoReplied = 'auto-replied';
    public const AutoNotified = 'auto-notified';
}
