<?php

namespace DannyCain\EmailIngestion\Enums;

class Permission
{
    // Permissions
    public const AuthenticateAccount = 'Authenticate account';

    // Assignable permissions
    public static $grantablePermissions = [
        self::AuthenticateAccount,
    ];
}
