<?php

namespace DannyCain\EmailIngestion\Support;

use Closure;
use DannyCain\EmailIngestion\Models\IncomingEmail;
use DannyCain\EmailIngestion\Routing\EmailRoute;

class EmailIngestion
{
    /**
     * @var Closure[]
     */
    protected $before = [];
    /**
     * @var Closure[]
     */
    protected $after = [];
    /**
     * @var EmailRoute[]
     */
    protected $routes = [];

    protected Closure|null $fallback = null;

    public function before(Closure $handler): EmailIngestion
    {
        $this->before[] = $handler;
        return $this;
    }

    public function after(Closure $handler): EmailIngestion
    {
        $this->after[] = $handler;
        return $this;
    }

    public function loadRoutesFrom(string $file)
    {
        include_once $file;
    }

    public function route(): EmailRoute
    {
        $route = app(EmailRoute::class);
        $this->routes[] = $route;

        return $route;
    }

    public function fallback(Closure $handler): EmailIngestion
    {
        $this->fallback = $handler;
        return $this;
    }

    public function dispatch(IncomingEmail $email)
    {
        try {
            foreach ($this->before as $handler) {
                $handler($email);
            }

            foreach ($this->routes as $route) {
                if (!$route->test($email)) {
                    continue;
                }

                $handler = $route->getHandler();
                $handler($email);
                $email->was_handled = true;
                if ($name = $route->getName()) {
                    $email->handled_by = $name;
                }

                break;
            }

            if (!$email->was_handled && $this->fallback) {
                $handler = $this->fallback;
                $handler($email);
            }

            foreach ($this->after as $handler) {
                $handler($email);
            }

            // this is updating the 'received_at' date to now, and I have no idea why
            $email->update([
                'processed_at' => now(),
            ]);
        } catch (\Throwable $e) {
            report($e);
            $email->update([
                'processed_at' => now(),
                'error' => $e->getMessage()."\n\n".$e->getTraceAsString(),
            ]);
        }
    }
}
