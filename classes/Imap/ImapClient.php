<?php

namespace DannyCain\EmailIngestion\Imap;

use DannyCain\EmailIngestion\Models\ImapAccount;
use DannyCain\EmailIngestion\Models\IncomingEmail;
use DannyCain\EmailIngestion\Support\EmailIngestion;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Psr\Log\LoggerInterface;
use Webklex\PHPIMAP\Address;
use Webklex\PHPIMAP\ClientManager;
use Webklex\PHPIMAP\Message;

class ImapClient
{
    public static ?LoggerInterface $logger = null;

    public function readInbox()
    {
        $clientManager = $this->clientManagerFactory();
        foreach ($this->accounts() as $account) {
            try {
                self::$logger?->info("Checking IMAP inbox for {$account->email}");
                if ($account->requires_refresh) {
                    // todo - get new access token
                }
                if ($account->error_count >= 5) {
                    self::$logger?->warning("Skipping {$account->email}, error cap reached.");
                    continue;
                }
                if (!$account->is_valid) {
                    self::$logger?->warning("Skipping {$account->email}, authentication is invalid.");
                    continue;
                }

                $folderName = $account->inbox_folder;;
                $client = $clientManager->make($account->toConfigArray());
                $client->connect();
                $folder = $client->getFolder($folderName);
                foreach ($folder->messages()->all()->get() as $message) {
                    try {
                        $this->parseEmail($account->email, $message);
                    } catch (\Throwable $e) {
                        $account->update([
                            'error_count' => $account->error_count + 1
                        ]);
                        report($e);
                    }
                }
                $account->update([
                    'error_count' => 0,
                ]);
            } catch (\Throwable $e) {
                $account->update([
                    'error_count' => $account->error_count + 1
                ]);
                self::$logger?->error($e->getMessage());
                report($e);
            }
        }
    }

    protected function parseEmail(string $account, Message $message)
    {
        $existing = IncomingEmail
            ::where('source', "imap")
            ->where('account', $account)
            ->where('message_id', $message->getMessageId())
            ->first();
        if ($existing) {
            self::$logger?->info("{$message->getMessageId()} already exists in the database.");
            return;
        }

        $rawHeaders = $message->getHeader()->raw;
        $rawBody = $message->getRawBody();
        $raw = $rawHeaders."\n\n".$rawBody;
        $jsonHeaders = [];
        foreach ($message->getHeader()->getAttributes() as $header) {
            $jsonHeaders[$header->getName()] = [
                [
                    'value' => $header->toString(),
                    'parts' => [(object)[], (object)[], (object)[]],
                    'raw_value' => $header->toString(),
                ]
            ];
        }

        /** @var Address $from */
        $from = $message->getFrom()->first();
        $deliveredTo = $message->getHeader()->get('delivered_to')?->first();
        /** @var Address[] $to */
        $to = $message->getTo()->all();
        $subject = $message->getSubject()->first();
        $autoSubmitted = $message->getHeader()->get('auto_submitted')->first();
        $html = $message->getHTMLBody();
        $text = $message->getTextBody();

        $email = IncomingEmail::create([
            'source' => "imap",
            'account' => $account,
            'message_id' => $message->getMessageId(),
            'from_name' => $from->personal,
            'from_email' => $from->mail,
            'delivered_to' => $deliveredTo,
            'json_to' => array_map(fn($recip) => ['name' => $recip->personal, 'email' => $recip->mail], $to),
            'subject' => $subject,
            'spf_status' => 'not-checked',
            'dkim_status' => 'not-checked',
            'auto_submitted' => $autoSubmitted,
            'json_headers' => $jsonHeaders,
            'json_attachments' => $this->saveAttachments($account, $message),
            'html_body' => $html,
            'text_body' => $text,
            'raw' => $raw,
            'started_processing_at' => now(),
            'received_at' => $message->getHeader()->get('Date')->first(),
        ]);

        app(EmailIngestion::class)->dispatch($email);
    }

    protected function saveAttachments(string $account, Message $message)
    {
        $attachments = [];
        $drive = \Storage::drive(config('email-ingestion.attachment.storage_drive'));
        $path = config('email-ingestion.attachment.storage_path');
        if (!$drive->exists($path)) {
            $drive->makeDirectory($path);
        }

        foreach ($message->getAttachments() as $attachment) {
            $filename = $attachment->getName();
            $extension = \File::extension($filename);
            $storageName = Str::random();
            if ($extension) {
                $storageName .= '.' . $extension;
            }

            $attachment->save($drive->path($path), $storageName);
            $attachments[] = [
                'name' => $filename,
                'path' => $path . '/' . $storageName,
            ];
        }

        return $attachments;
    }

    /**
     * @return Collection|ImapAccount[]
     */
    protected function accounts(): Collection
    {
        return ImapAccount
            ::where('is_active', true)
            ->orderBy('email')
            ->get();
    }

    protected function clientManagerFactory(): ClientManager
    {
        return new ClientManager($this->loadConfig());
    }

    protected function loadConfig(): array
    {
        return once(function() {
            $config = config('email-ingestion.imap');
            return $config;
        });
    }
}
