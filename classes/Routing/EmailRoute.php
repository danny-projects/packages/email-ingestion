<?php

namespace DannyCain\EmailIngestion\Routing;

use Closure;
use DannyCain\EmailIngestion\Models\IncomingEmail;
use Illuminate\Support\Facades\Log;

class EmailRoute
{
    protected $preconditions = [];
    protected Closure|null $handler = null;
    protected string $name = '';

    public function name(string $name): EmailRoute
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function to(string $pattern): EmailRoute
    {
        $this->preconditions[] = ['field' => 'to', 'test' => $pattern];
        return $this;
    }

    public function subject(string $pattern): EmailRoute
    {
        $this->preconditions[] = ['field' => 'subject', 'test' => $pattern];
        return $this;
    }

    public function from(string $pattern): EmailRoute
    {
        $this->preconditions[] = ['field' => 'from', 'test' => $pattern];
        return $this;
    }

    public function body(string $pattern): EmailRoute
    {
        $this->preconditions[] = ['field' => 'body', 'test' => $pattern];
        return $this;
    }

    public function handle(Closure $handler): EmailRoute
    {
        $this->handler = $handler;

        return $this;
    }

    public function getHandler(): Closure
    {
        return $this->handler ?? static fn(IncomingEmail $email) => null;
    }

    public function test(IncomingEmail $email): bool
    {
        foreach ($this->preconditions as $precondition) {
            $value = match($precondition['field']) {
                'from' => $email->from_email,
                'to' => $email->json_to,
                'subject' => $email->subject,
                'body' => $email->body,
                default => null,
            };

            if (!$this->testPrecondition($precondition['test'], $value)) {
                if (config('email-ingestion.routing.log')) {
                    $routeName = $this->name ?: 'Unnamed';
                    $value = $value ?? [];
                    if (!is_array($value)) {
                        $value = ['value' => $value];
                    }
                    Log::info("Routing {$email->id} failed precondition '{$precondition['test']}' for route '{$routeName}'", $value);
                }
                return false;
            }
        }

        return true;
    }

    protected function testPrecondition($test, $value)
    {
        if (is_array($value)) {
            foreach ($value as $subvalue) {
                if ($this->testPrecondition($test, $subvalue)) {
                    return true;
                }
            }
            return false;
        }


        if (str_starts_with($test, '/') && str_ends_with($test, '/')) {
            return preg_match($test, $value);
        }
        return $test === $value;
    }
}
