<?php

namespace DannyCain\EmailIngestion\Jobs;

use DannyCain\EmailIngestion\Models\IncomingEmail;
use DannyCain\EmailIngestion\Support\EmailIngestion;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Mika56\SPFCheck\DNS\DNSRecordGetter;
use Mika56\SPFCheck\Model\Query;
use Mika56\SPFCheck\Model\Result;
use Mika56\SPFCheck\SPFCheck;
use PHPMailer\DKIMValidator\Validator;
use ZBateson\MailMimeParser\Header\AddressHeader;
use ZBateson\MailMimeParser\Header\Part\AddressPart;
use ZBateson\MailMimeParser\Message;

class ProcessEmailJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(public IncomingEmail $email)
    {

    }

    public function handle()
    {
        $email = $this->email;
        $this->startProcessing($email);

        $parsed = Message::from($email->raw, true);
        /** @var AddressHeader $to */
        $to = $parsed->getHeader('To');
        /** @var AddressHeader $from
         */
        $from = $parsed->getHeader('From');
        $subject = $parsed->getHeaderValue('Subject');
        $dkimStatus = $this->validateDkim($email);
        $spfStatus = $this->validateSpf($email, $parsed);

        $attachments = $this->saveAttachments($parsed);
        $headers = [];
        foreach ($parsed->getAllHeaders() as $header) {
            $headers[$header->getName()] = $headers[$header->getName()] ?? [];
            $headers[$header->getName()][] = [
                'value' => $header->getValue(),
                'parts' => $header->getParts(),
                'raw_value' => $header->getRawValue(),
            ];
        }

        $recipients = array_map(fn(AddressPart $recipient) => [
            'name' => $recipient->getName(),
            'email' => $recipient->getEmail(),
        ], $to?->getAddresses() ?? []);

        $autoSubmitted = $email->firstHeaderValue('Auto-Submitted');
        if ($autoSubmitted === 'no') {
            $autoSubmitted = null;
        }

        $email->update([
            'json_to' => $recipients,
            'from_name' => $from?->getPersonName(),
            'from_email' => $from?->getEmail(),
            'delivered_to' => $parsed->getHeaderValue('X-Original-To'),
            'subject' => $subject,
            'dkim_status' => $dkimStatus,
            'spf_status' => $spfStatus,
            'auto_submitted' => $autoSubmitted,
            'html_body' => $parsed->getHtmlContent(),
            'text_body' => $parsed->getTextContent(),
            'json_headers' => $headers,
            'json_attachments' => $attachments,
        ]);

        app(EmailIngestion::class)->dispatch($email);
    }

    protected function saveAttachments(Message $parsed): array
    {
        $attachments = [];
        $drive = \Storage::drive(config('email-ingestion.attachment.storage_drive'));
        $path = config('email-ingestion.attachment.storage_path');
        if (!$drive->exists($path)) {
            $drive->makeDirectory($path);
        }

        foreach ($parsed->getAllAttachmentParts() as $attachment) {
            $filename = $attachment->getFilename();
            $extension = \File::extension($filename);
            $storageName = Str::random() . '.' . $extension;

            $attachment->saveContent($drive->path($path . '/' . $storageName));
            $attachments[] = [
                'name' => $filename,
                'path' => $path . '/' . $storageName,
            ];
        }
        return $attachments;
    }

    protected function validateSpf(IncomingEmail $email, Message $parsed): string
    {
        $path = $this->extractTransitPath($parsed);
        $lastRelay = $path[0] ?? null;
        if (!$lastRelay) {
            return IncomingEmail::NotChecked;
        }

        $senderIP = $lastRelay['ip'] ?? null;
        if (!$senderIP) {
            return IncomingEmail::NotChecked;
        }

        $sender = $parsed->getHeaderValue('From');
        $domain = explode('@', $sender)[1] ?? null;

        $validator = new SPFCheck(new DNSRecordGetter());
        return match ($validator->getResult(new Query($senderIP, $domain))->getResult()) {
            Result::PASS => IncomingEmail::Valid,
            Result::NEUTRAL => IncomingEmail::NotChecked,
            Result::FAIL, Result::SOFTFAIL => IncomingEmail::Invalid,
            default => IncomingEmail::Error,
        };
    }

    protected function extractTransitPath(Message $parsed): array
    {
        $ipPattern = '(?:\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\])';
        $keyValuePattern = '(from|by|for) ([^\s;]+)';

        $path = [];
        foreach ($parsed->getAllHeadersByName('Received') as $header) {
            $hop = [];
            foreach (explode("\n", $header->getValue()) as $line) {
                $line = trim($line);
                preg_match("/{$keyValuePattern}/", $line, $matches);
                $key = $matches[1] ?? null;
                $value = $matches[2] ?? null;

                preg_match("/{$ipPattern}/", $line, $matches);
                $ip = $matches[1] ?? null;

                if (!$key) {
                    Log::warning("Unparsed 'Received' line: '{$line}");
                    continue;
                }
                if (!in_array($key, ['from', 'by', 'for'], true)) {
                    Log::warning("Unrecognised 'Received' key: '{$key}");
                    continue;
                }
                $hop[$key] = $value;
                if ($key === 'from' && $ip) {
                    $hop['ip'] = $ip;
                }
            }
            $path[] = $hop;
        }

        return $path;
    }

    protected function validateDkim(IncomingEmail $email): string
    {
        try {
            $validator = new Validator($email->raw);
            if ($validator->validate()) {
                return $validator->validateBoolean() ? IncomingEmail::Valid : IncomingEmail::Invalid;
            }
        } catch (Throwable $e) {
            report($e);
            return IncomingEmail::Error;
        }

        return IncomingEmail::NotChecked;
    }

    protected function startProcessing(IncomingEmail $email): bool
    {
        $affected = IncomingEmail
            ::whereNull('started_processing_at')
            ->where('id', $email->id)
            ->update([
                'started_processing_at' => now(),
            ]);

        return (bool)$affected;
    }
}
