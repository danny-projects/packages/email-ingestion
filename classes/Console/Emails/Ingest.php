<?php

namespace DannyCain\EmailIngestion\Console\Emails;

use DannyCain\EmailIngestion\Jobs\ProcessEmailJob;
use DannyCain\EmailIngestion\Models\IncomingEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class Ingest extends Command
{
    protected $signature = '
        emails:ingest
    ';

    public function handle()
    {
        Log::info('received email');
        $content = '';
        while (!feof(STDIN)) {
            $content .= fread(STDIN, 1024);
        }

        $email = IncomingEmail::create([
            'raw' => $content,
            'received_at' => now(),
        ]);
        ProcessEmailJob::dispatch($email);
    }
}
