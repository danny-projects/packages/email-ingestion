<?php

namespace DannyCain\EmailIngestion\Console\Emails;


use DannyCain\EmailIngestion\Imap\ImapClient;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class Imap extends Command
{
    protected $signature = '
        emails:imap
    ';

    public function handle()
    {
        ImapClient::$logger = Log::getLogger();
        app(ImapClient::class)->readInbox();
    }
}
