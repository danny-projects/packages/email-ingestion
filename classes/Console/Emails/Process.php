<?php

namespace DannyCain\EmailIngestion\Console\Emails;

use DannyCain\EmailIngestion\Jobs\ProcessEmailJob;
use DannyCain\EmailIngestion\Models\IncomingEmail;
use DannyCain\EmailIngestion\Support\EmailIngestion;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Mika56\SPFCheck\DNS\DNSRecordGetter;
use Mika56\SPFCheck\Model\Query;
use Mika56\SPFCheck\Model\Result;
use Mika56\SPFCheck\SPFCheck;
use PHPMailer\DKIMValidator\Validator;
use Throwable;
use ZBateson\MailMimeParser\Header\AddressHeader;
use ZBateson\MailMimeParser\Header\Part\AddressPart;
use ZBateson\MailMimeParser\Message;

use Illuminate\Console\Command;

class Process extends Command
{
    protected $signature = '
        emails:process
        {emailID?}
    ';

    public function handle()
    {
        $this->warn('This is no longer required');
        return self::FAILURE;
        
        $id = $this->argument('emailID');
        if ($id) {
            if ($id === 'unprocessed') {
                $emails = IncomingEmail::where('was_handled', false)->get();
            } else {
                $emails = IncomingEmail::where('id', $id)->get();
            }

            foreach ($emails as $email) {
                $email->update([
                    'error' => null,
                    'started_processing_at' => null,
                ]);
                $this->processAndDispatch($email);
            }
            return;
        }

        while($email = $this->next()) {
            $this->processAndDispatch($email);
        }
    }

    protected function processAndDispatch(IncomingEmail $email)
    {
        try {
            ProcessEmailJob::dispatch($email);
        } catch (Throwable $e) {
            report($e);
            $email->update([
                'processed_at' => now(),
                'error' => $e->getMessage() . "\n\n" . $e->getTraceAsString(),
            ]);
        }
    }

    protected function process(IncomingEmail $email)
    {
        $parsed = Message::from($email->raw, true);
        /** @var AddressHeader $to */
        $to = $parsed->getHeader('To');
        /** @var AddressHeader $from
         */
        $from = $parsed->getHeader('From');
        $subject = $parsed->getHeaderValue('Subject');
        $dkimStatus = $this->validateDkim($email);
        $spfStatus = $this->validateSpf($email, $parsed);

        $attachments = $this->saveAttachments($parsed);
        $headers = [];
        foreach ($parsed->getAllHeaders() as $header) {
            $headers[$header->getName()] = $headers[$header->getName()] ?? [];
            $headers[$header->getName()][] = [
                'value' => $header->getValue(),
                'parts' => $header->getParts(),
                'raw_value' => $header->getRawValue(),
            ];
        }

        $recipients = array_map(fn(AddressPart $recipient) => [
            'name' => $recipient->getName(),
            'email' => $recipient->getEmail(),
        ], $to?->getAddresses() ?? []);

        $email->update([
            'json_to' => $recipients,
            'from_name' => $from?->getPersonName(),
            'from_email' => $from?->getEmail(),
            'delivered_to' => $parsed->getHeaderValue('X-Original-To'),
            'subject' => $subject,
            'dkim_status' => $dkimStatus,
            'spf_status' => $spfStatus,
            'html_body' => $parsed->getHtmlContent(),
            'text_body' => $parsed->getTextContent(),
            'json_headers' => $headers,
            'json_attachments' => $attachments,
        ]);
    }

    protected function next(): ?IncomingEmail
    {
        return IncomingEmail
            ::orderBy('received_at', 'ASC')
            ->whereNull('started_processing_at')
            ->first();
    }
}
