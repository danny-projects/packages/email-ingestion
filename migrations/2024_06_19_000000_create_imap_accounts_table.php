<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imap_accounts', function (Blueprint $table) {
            $table->id();

            $table->string('email');
            $table->string('inbox_folder')->default('INBOX');
            $table->boolean('is_active')->default(true);

            $table->string('auth_type'); // password, oauth
            $table->string('host');
            $table->string('username');

            $table->string('encrypted_password')->nullable();
            $table->longText('encrypted_access_token')->nullable();
            $table->longText('encrypted_refresh_token')->nullable();
            $table->dateTime('token_expires_at')->nullable();
            $table->longText('json_configuration')->nullable();

            $table->integer('error_count')->default(0);
            $table->dateTime('last_checked_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imap_accounts');
    }
};
