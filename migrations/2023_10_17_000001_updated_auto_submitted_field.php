<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $emails = \DannyCain\EmailIngestion\Models\IncomingEmail::orderBy('id')->cursor();
        foreach ($emails as $email) {
            $autoSubmitted = $email->firstHeaderValue('Auto-Submitted');
            if ($autoSubmitted === 'no') {
                $autoSubmitted = null;
            }

            $email->update([
                'auto_submitted' => $autoSubmitted,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
};
