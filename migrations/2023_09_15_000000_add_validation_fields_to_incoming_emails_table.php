<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incoming_emails', function (Blueprint $table) {
            $table->string('spf_status')->default('not-checked')->after('subject');
            $table->string('dkim_status')->default('not-checked')->after('spf_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumns('incoming_emails', ['spf_status', 'dkim_status']);
    }
};
