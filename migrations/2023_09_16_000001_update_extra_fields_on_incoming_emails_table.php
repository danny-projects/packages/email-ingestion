<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use ZBateson\MailMimeParser\Message;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $emails = \DannyCain\EmailIngestion\Models\IncomingEmail::whereNotNull('processed_at')->orderBy('id')->get();

        foreach ($emails as $email) {
            $parsed = Message::from($email->raw, true);
            $headers = [];
            foreach ($parsed->getAllHeaders() as $header) {
                $headers[$header->getName()] = $headers[$header->getName()] ?? [];
                $headers[$header->getName()][] = [
                    'value' => $header->getValue(),
                    'parts' => $header->getParts(),
                    'raw_value' => $header->getRawValue(),
                ];
            }

            $email->update([
                'delivered_to' => $parsed->getHeaderValue('X-Original-To'),
                'json_headers' => $headers,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumns('incoming_emails', ['delivered_to', 'json_headers']);
    }
};
