<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incoming_emails', function (Blueprint $table) {
            $table->id();

            $table->string('from_name')->nullable();
            $table->string('from_email')->nullable();
            $table->longText('json_to')->nullable();
            $table->string('subject')->nullable();
            $table->longText('html_body')->nullable();
            $table->longText('text_body')->nullable();
            $table->longText('json_attachments')->nullable();
            $table->boolean('was_handled')->default(false);
            $table->string('handled_by')->nullable();
            $table->longText('error')->nullable();

            $table->longText('raw');
            $table->timestamp('received_at');
            $table->timestamp('started_processing_at')->nullable();
            $table->timestamp('processed_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incoming_emails');
    }
};
