<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incoming_emails', function (Blueprint $table) {
            $table->string('source')->after('id')->default('ingestion');
            $table->string('account')->nullable()->after('source');
            $table->string('message_id')->nullable()->after('account');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumns('incoming_emails', ['source', 'account', 'message_id']);
    }
};
