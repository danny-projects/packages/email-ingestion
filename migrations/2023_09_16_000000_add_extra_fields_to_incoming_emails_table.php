<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incoming_emails', function (Blueprint $table) {
            $table->string('delivered_to')->nullable()->after('from_email');
            $table->longText('json_headers')->nullable()->after('text_body');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumns('incoming_emails', ['delivered_to', 'json_headers']);
    }
};
