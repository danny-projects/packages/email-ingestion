<?php

Route::middleware(['web'])->group(function() {
    foreach (glob(__DIR__ . '/routes/web/*.php') as $file) {
        require $file;
    }
});

Route::middleware(['api'])->prefix('api')->group(function() {
    foreach (glob(__DIR__ . '/routes/api/*.php') as $file) {
        require $file;
    }
});
