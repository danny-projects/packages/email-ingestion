<?php

Route::middleware(['auth'])->group(function() {
    Route::get('/auth', [\DannyCain\EmailIngestion\Controllers\OauthController::class, 'redirect'])->name('oauth.redirect');
    Route::get('/auth/{provider}', [\DannyCain\EmailIngestion\Controllers\OauthController::class, 'auth'])->name('oauth.callback');
});
