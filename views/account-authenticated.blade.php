@extends(config('laravel-utilities.public.layout'))

@section(config('laravel-utilities.public.section-content'))
    <h1>Authentication Successful</h1>
    <p>
        You have successfully authenticate the email address {{ $account->email }}.
    </p>
@endsection
